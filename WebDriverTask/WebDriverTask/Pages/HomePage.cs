﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace WebDriverTask.Pages
{
    public class HomePage:BasePage
    {
        private readonly IWebDriver browse;

        public IWebElement search_line
        {
            get
            {
                return browse.FindElement(By.XPath("//input[contains(@title,'Поиск')]"));
            }

        }

        public IWebElement search_button
        {
            get 
            {
                return browse.FindElement(By.XPath("//button[contains(@aria-label,'Поиск в Google')]"));      
            }
        }

        public HomePage(IWebDriver driver) : base(driver)
        { 
            this.browse = driver; 
        }

        public bool IsSearchLineVisiable()
        {
            return search_line.Displayed;
        }
        public bool IsSearchButtonVisiable()
        {
            return search_button.Displayed;
        }
        public void clickSeachButton()
        {
            search_button.Click();
        }

        public void writeToSearch(string word)
        {
            search_line.Clear();
            search_line.SendKeys(word);
        }
    }
}

//: base(driver)
