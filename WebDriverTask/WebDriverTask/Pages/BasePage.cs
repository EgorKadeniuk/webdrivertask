﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebDriverTask.Pages
{
   public class BasePage
    {
        private readonly IWebDriver driver;
        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void implicitWait(long timetoWait)=> driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timetoWait);

    }
}
