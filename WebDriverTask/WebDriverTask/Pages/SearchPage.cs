﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
namespace WebDriverTask.Pages
{
    public class SearchPage:BasePage
    {
        private readonly IWebDriver browse;
        public IWebElement image_cotent
        {
            get
            {
                return browse.FindElement(By.XPath("//img[contains(@alt,'house cat')]"));
            }
        }

        public SearchPage(IWebDriver driver) : base(driver) 
        {
            browse = driver;
        }


    }
}
