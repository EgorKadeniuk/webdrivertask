﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Text;

namespace WebDriverTask.Test
{
    public class SearchImageTest : BaseTest
    {
        private readonly string word = "cat";

        [Test]
        public void SearchImage()
        {
            GetHomePage().IsSearchButtonVisiable();
            GetHomePage().IsSearchLineVisiable();
            GetHomePage().writeToSearch(word);
            GetHomePage().clickSeachButton();
            Assert.IsTrue(GetSearchPage().image_cotent.Displayed);
        }

    }
}
