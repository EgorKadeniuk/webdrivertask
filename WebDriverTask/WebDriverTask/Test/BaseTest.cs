using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using WebDriverTask.Pages;

namespace WebDriverTask
{
    public class BaseTest
    {
        private IWebDriver driver;

        private readonly string image_url = "https://images.google.com/";
        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.FullScreen();
            driver.Navigate().GoToUrl(image_url);
        }

        public IWebDriver GetDriver()
        {
            return driver;
        }

        public HomePage GetHomePage()
        {
            return new HomePage(GetDriver());
        }

        public SearchPage GetSearchPage()
        {
            return new SearchPage(GetDriver());
        }


        [TearDownAttribute]
        public void AfterDown() => driver.Close();
    }
}